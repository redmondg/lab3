package com.example.lab3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;


import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {


		ApplicationContext context = SpringApplication.run(Application.class, args);

		CartService service = context.getBean(CartServiceImpl.class);
		service.addToCart(0,1);
		service.addToCart(1,2);
		service.addToCart(2,1);

		service.removeFromCart(2);

		double totalCost = service.calculateCartCost();
		System.out.printf("Total cart cost = %.2f", totalCost);

	}

	@Bean
	public Map<Integer, Item> catalog(){

		Map<Integer, Item> items = new HashMap<>();
		items.put(0, new Item(0, "Camera", 1599.00));
		items.put(1, new Item(1, "Laptop", 2199.99));
		items.put(2, new Item(2, "Car", 43000.59));


		return items;
	}
}
