package com.example.lab3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CartServiceImpl implements CartService{
    @Autowired
    private CartRepository repo;
    @Value("#{catalog}")
    private Map<Integer, Item> catalog = new HashMap<>();


    @Override
    public void addToCart(int id, int num) {
        if(catalog.containsKey(id)){
            repo.add(id, num);
        }
    }

    @Override
    public void removeFromCart(int id) {
        repo.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repo.getAll();
    }


    @Override
    public double calculateCartCost() {
        Map<Integer, Integer> items = repo.getAll();
        double totalCost = 0;
        for(Map.Entry<Integer, Integer> item : items.entrySet()){
            totalCost += catalog.get(item.getKey()).getPrice() * item.getValue();
        }
        return totalCost;
    }
}
