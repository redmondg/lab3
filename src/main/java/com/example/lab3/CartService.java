package com.example.lab3;

import java.util.Map;

public interface CartService {

    void addToCart(int id, int num);
    void removeFromCart(int id);
    Map<Integer, Integer> getAllItemsInCart();
    double calculateCartCost();


}
